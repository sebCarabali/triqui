=begin
Define la clase Board que representa el tablero del triqui.

+- - - - - -+
| 1 | 2 | 3 |
+- - - - - -+
| 4 | 5 | 6 |
+- - - - - -+
| 7 | 8 | 9 |
+- - - - - -+

=end

class Board
    
    EMPTY_POS = ' '

    def initialize(current_player)
        @current_player = current_player
        # Inicializamos la matriz que representa el tablero.
        @board = Array.new(3){
            Array.new(3){ EMPTY_POS }
        }
    end

    def print_board
        # Imprime el trablero.
        puts "+- - - - - -+"
        for i in (0..2) do
            print "| "
            for j in (0..2) do
                if @board[i][j] == EMPTY_POS
                    print j + (i*3) + 1
                else
                    print @board[i][j]
                end
                print " | "
            end
            puts "\n+- - - - - -+"
        end
    end

    def is_full
        # Verifica si el tablero esta lleno, esto es que no hay ninguna casilla igual a EMPTY_POS
        for i in (0..2) do
            for j in (0..2) do
                if @board[i][j] == EMPTY_POS then return false end
            end
        end
        return true
    end

    def make_move(current_player)
        # Realiza un movimiento del usuario.
        done = false
        until done # equivalente while !done
            print "Jugador " + current_player + ": Donde quieres juegar. ?"
            mov = gets.to_i - 1 # se le pide al usuario la casilla a la cual movera
            col = mov % @board.size # obtenemos la columna
            row = (mov - col) / @board.size # obtenemos la fila
            if validate_pos(row, col) # se valida que el movimiento sea correcto
                @board[row][col] = current_player
                done = true
            end
        end        
    end

    def validate_pos(row, col)
        # verifica row y col sean menores que el tamaño del tablero y si la casilla boars[row][col] esta libre
        puts " row: #{row}, col: #{col}"
        if row < 3 and col < 3 then
            if @board[row][col] == EMPTY_POS then
                return true
            else
                puts "Posición ocupada."
            end
        end
        puts "Posicion no valida."
        return false
    end

    def get_next_turn
        # Cambia de turno.
        if @current_player == 'X'
            @current_player = 'O'
        else
            @current_player = 'X'
        end
        return @current_player
    end
    
    # Accessor para el tablero.
    attr_reader :board
end
