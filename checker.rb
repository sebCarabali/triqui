
class Checker

    def check_win(board)
        # Verifica si existe un ganador en el juego.
        winer = win_rows(board)
        if winer
            return winer
        end
        winer = win_cols(board)
        if winer
            return winer
        end
        winer = win_diag(board)
        if winer
            return winer
        end
        return
    end

    def win_rows(b)
        # Verifica si algún juador gana por filas.
        for row in (0..2)
            player = b.board[row][0]
            for col in (1..2)
                if player != b.board[row][col]
                    break
                elsif col == 2 and player != ' '
                    return player
                end
            end
        end
        return
    end

    def win_cols(b)
        # Verifica si algún jugador gana por columnas.
        for col in (0..2)
            player = b.board[0][col]
            for row in (1..2)
                if player != b.board[row][col]
                    break
                elsif row == 2 and player != ' '
                    return player
                end
            end
        end
        return
    end

    def win_diag(b)
        # Verifica si algún jugador gana por las diagonales.
        player = b.board[0][0]
        for i in (1..2)
            if player != b.board[i][i]
                break
            elsif i == 2 and player != ' '
                return player
            end
        end

        col = 2
        row = 0
        player = b.board[row][col]

        while row < 2
            row += 1
            col -= 1
            if player !=  b.board[row][col]
                break
            elsif row == 2 and player != ' '
                return player
            end
        end
        return
    end

end
