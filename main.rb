=begin
    Importamos los elementos necesarios la clase Board que contiene toda
    la funcionalidad del tablero de triqui y la clase Checker que contiene
    métodos para verificar si un jugador ganó el juego.
=end
require_relative 'board'
require_relative 'checker'

current_player = 'X'
board = Board.new current_player
checker = Checker.new
board.print_board
winer = ''

# Cilco de juego.
until board.is_full # equivalente a while !board.is_full
    board.make_move(current_player)
    current_player = board.get_next_turn
    board.print_board
    winer = checker.check_win(board)
    if winer then break end
    puts
end

if winer
    print "El ganador es #{winer}"
else
    print "Es empate."
end
